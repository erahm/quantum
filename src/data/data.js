const options = [
  {
    label: "User Email",
    name: "email",
    value: "user_email",
    type: "string",
  },
  {
    label: "Screen Width",
    name: "screenWidth",
    value: "screen_width",
    type: "number",
  },
  {
    label: "Screen Height",
    name: "screenHeight",
    value: "screen_weight",
    type: "number",
  },
  {
    label: "# of Visits",
    name: "visits",
    value: "visits",
    type: "number",
  },
  {
    label: "First Name",
    name: "first",
    value: "user_first_name",
    type: "string",
  },
  {
    label: "Last Name",
    name: "last",
    value: "user_last_name",
    type: "string",
  },
  {
    label: "Page Response Time",
    name: "responseTime",
    value: "page_response",
    type: "number",
  },
  {
    label: "Domain",
    name: "domain",
    value: "domain",
    type: "string",
  },
  {
    label: "Page Path",
    name: "path",
    value: "path",
    type: "string",
  },
];

const operators = [
  {
    type: "string",
    data: [
      {
        label: "equals",
        value: "equals",
        operator: (param) => `${param.column} = '${param.value}'`,
      },
      {
        label: "contains",
        value: "contains",
        operator: (param) => {
          return `CONTAINS(${param.column}, ${param.value})`;
        },
      },
      {
        label: "starts with",
        value: "starts_with",
        operator: (param) => `${param.column} LIKE ${param.value}%`,
      },
      {
        label: "in list",
        value: "in_list",
        operator: (param) => {
          return `${param.column} IN (${param.value})`;
        },
      },
    ],
  },
  {
    type: "number",
    data: [
      {
        label: "equals",
        value: "equals",
        stopWords: [],
        operator: (param) => `${param.column} = ${param.value}`,
      },
      {
        label: "between",
        value: "between",
        stopWords: ["is", "and"],
        operator: (param) => {
          return `${param.column} BETWEEN ${param.value} AND ${param.value2}`;
        },
      },
      {
        label: "greater than",
        value: "greater_than",
        stopWords: ["is"],
        operator: (param) => {
          return `${param.column} > ${param.value}`;
        },
      },
      {
        label: "less than",
        value: "less_than",
        stopWords: ["is"],
        operator: (param) => {
          return `${param.column} < ${param.value}`;
        },
      },
      {
        label: "in list",
        value: "in_list",
        stopWords: ["is"],
        operator: (param) => {
          return `${param.column} IN (${param.value})`;
        },
      },
    ],
  },
];

module.exports.options = options;
module.exports.operators = operators;
