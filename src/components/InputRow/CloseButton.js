import React from "react";
import styles from "./close-button.scss";

const CloseButton = (props) => {
  const { handleClick } = props;

  return (
    <div onClick={handleClick} className="close-wrapper">
      <div className="close"></div>
    </div>
  );
};

export default CloseButton;
