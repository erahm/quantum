import React, { useState } from "react";
import Select from "../Select";

const NumberInputs = (props) => {
  const { handleChange, operators, rowId } = props;
  const options = operators[0].data;

  const [operator, setOperator] = useState({ stopWords: [] });
  const [inputValue, setInputValue] = useState("");
  const [inputValue2, setInputValue2] = useState("");

  const renderStopWord = (word) => {
    return <div className="stop-word">{word}</div>;
  };

  return (
    <div className="inputs-wrapper">
      {operator.stopWords[0] ? renderStopWord(operator.stopWords[0]) : ""}
      <Select
        key={`operator-${rowId}`}
        name="operator"
        options={options}
        onChange={(event) => {
          let matches = options.filter(
            (option) => option.value == event.target.value
          );
          setOperator(matches[0]);
          handleChange(event);
        }}
      />
      <input
        type="text"
        name="value"
        placeholder="Enter Value"
        value={inputValue}
        onChange={(event) => {
          handleChange(event);
          setInputValue(event.target.value);
        }}
      ></input>
      {operator.stopWords[1] ? renderStopWord(operator.stopWords[1]) : ""}
      {operator.stopWords[1] ? (
        <input
          type="text"
          name="value2"
          placeholder="Enter Value"
          value={inputValue2}
          onChange={(event) => {
            handleChange(event);
            setInputValue2(event.target.value);
          }}
        ></input>
      ) : (
        ""
      )}
    </div>
  );
};

export default NumberInputs;
