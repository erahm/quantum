import React, { useState } from "react";
import CloseButton from "./CloseButton";
import Select from "../Select";
import StringInputs from "./StringInputs";
import NumberInputs from "./NumberInputs";
import { options, operators } from "../../data/data";
import styles from "./row.scss";

const InputRow = (props) => {
  const {
    removeRow,
    row: { id },
    handleChange,
  } = props;

  const [type, setType] = useState(undefined);

  const determineType = (event) => {
    const option = options.filter(
      (option) => option.value == event.target.value
    )[0];

    setType(option.type);
  };

  const renderInputs = () => {
    let inputs;

    if (type === "string") {
      inputs = (
        <StringInputs
          rowId={id}
          operators={operators.filter((operator) => operator.type === "string")}
          handleChange={handleChange}
        />
      );
    } else {
      inputs = (
        <NumberInputs
          rowId={id}
          operators={operators.filter((operator) => operator.type === "number")}
          handleChange={handleChange}
        />
      );
    }

    return inputs;
  };

  return (
    <div id={id} className="row-wrapper">
      <CloseButton handleClick={removeRow} />
      <Select
        key={`column-${id}`}
        name="column"
        options={options}
        onChange={(e) => {
          determineType(e);
          handleChange(e);
        }}
      ></Select>
      {type && renderInputs()}
    </div>
  );
};

export default InputRow;
