import React, { useState } from "react";
import Select from "../Select";

const StringInputs = (props) => {
  const { handleChange, operators, rowId } = props;
  const [inputValue, setInputValue] = useState("");
  const options = operators[0].data;

  return (
    <div className="inputs-wrapper">
      <Select
        key={`operator-${rowId}`}
        name="operator"
        options={options}
        onChange={handleChange}
      />
      <input
        type="text"
        name="value"
        placeholder="Enter Value"
        value={inputValue}
        onChange={(event) => {
          handleChange(event);
          setInputValue(event.target.value);
        }}
      ></input>
    </div>
  );
};

export default StringInputs;
