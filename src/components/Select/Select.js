import React, { useState } from "react";
import styles from "./select.scss";

const renderOptions = (options) => {
  return options.map((option) => {
    return (
      <option key={option.value} value={option.value}>
        {option.label}
      </option>
    );
  });
};

const Select = (props) => {
  const {
    className,
    defaultValue = "",
    disabled,
    label = "Select Option",
    name,
    onChange = (event) => {},
    options = [],
  } = props;

  const [value, setValue] = useState(props.value);

  return (
    <div className="select-wrapper">
      <select
        className={className}
        disabled={disabled}
        name={name}
        onChange={(event) => {
          setValue(event.target.value);
          onChange(event);
        }}
        value={value}
      >
        {label && <option value={defaultValue}>{label}</option>}
        {renderOptions(options)}
      </select>
      <i class="fa fa-angle-down"></i>
    </div>
  );
};

export default Select;
