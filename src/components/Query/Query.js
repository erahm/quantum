import React from "react";
import styles from "./query.scss";

const Query = (props) => {
  const { query = "Your Generated SQL goes here:" } = props;

  return (
    <div className="query-wrapper">
      <div className="query-text">{query}</div>
    </div>
  );
};

export default Query;
