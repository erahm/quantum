import { options, operators } from "../data/data";

const table = "session";

const addColumnToSelect = (columns, column) => {
  const alreadyExists = columns.find((index) => index === column);

  if (!alreadyExists) {
    columns.push(column);
  }
};

const generateWhereClauseChunk = (where, param) => {
  const option = options.filter((option) => option.value == param.column)[0];
  const type = option.type;
  const data = operators.filter((item) => item.type == type)[0].data;
  const operator = data.filter((item) => item.value == param.operator)[0];

  where.push(operator.operator(param));
};

export default {
  generateQuery: (params) => {
    let columns = [];
    let where = [];
    let query = "";
    params.map((param) => {
      addColumnToSelect(columns, param.column);
      generateWhereClauseChunk(where, param);
    });

    query = "SELECT ".concat(columns.join(", "));
    query = query.concat(` FROM ${table}`);
    query = query.concat(` WHERE ${where.join(" AND ")}`);
    query.concat(";");

    return query;
  },
};
