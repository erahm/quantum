import React, { useState } from "react";
import InputRow from "./components/InputRow";
import Query from "./components/Query";
import QueryService from "./services/query";
import styles from "./app.scss";

var idIncrement = 0;

const generateDefaultRow = () => {
  idIncrement = idIncrement + 1;

  return {
    id: idIncrement,
    value: {},
  };
};

const App = (props) => {
  const [rows, setRows] = useState([generateDefaultRow()]);
  const [query, setQuery] = useState(undefined);

  const addRow = () => {
    const rowsCopy = [...rows];
    rowsCopy.push(generateDefaultRow());

    setRows(rowsCopy);
  };

  const generateQuery = () => {
    let params = [];
    rows.map((row) => {
      params.push(row.value);
    });

    setQuery(QueryService.generateQuery(params));
  };

  const removeRow = (index) => {
    const rowsCopy = [...rows];
    rowsCopy.splice(index, 1);

    if (rowsCopy.length === 0) {
      rowsCopy.push(generateDefaultRow());
    }

    setRows(rowsCopy);
    setQuery(undefined);
  };

  const resetRows = () => {
    setRows([generateDefaultRow]);
    setQuery(undefined);
  };

  const renderRows = (rows) => {
    return rows.map((row, index) => {
      return (
        <InputRow
          key={row.id}
          removeRow={removeRow.bind(index)}
          row={row}
          handleChange={(e) => {
            row.value[e.target.name] = e.target.value;
            console.log(row);
          }}
        />
      );
    });
  };

  return (
    <div className="app-wrapper">
      <div className="app-header">Search for Sessions</div>
      {renderRows(rows)}
      <button type="button" className="blue-button" onClick={addRow}>
        And
      </button>
      <div className="result-wrapper">
        <button type="button" className="blue-button" onClick={generateQuery}>
          <i class="fa fa-search"></i>
          Search
        </button>
        <button type="button" className="grey-button" onClick={resetRows}>
          Reset
        </button>
        <Query query={query} />
      </div>
    </div>
  );
};

export default App;
