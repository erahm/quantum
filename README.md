My approach to this project was to layout the general markup needed with some basic styling, get the functionaliy working, and then go back in and fine tune the styling. I saved the search icon on the `search` button and the chevrons on the `select` inputs for last becaue I knew they'd take the longest from having made them before.

I had started to create a search icon component where I was creating an icon out of an element with enough `border-radius` to make it a circle and another thin rectangle that was rotated to look like a handle. However, I was getting pretty tired so I scrapped this and just pulled in FontAwesome instead.

I also used FontAwesome for a chevron. I had tried several times to do something like:

```
select::after {
  border-width: 0 1px 1px 0;
  border: solid rgb(46, 58, 70);
  transition: rotate(45deg);
  -webkit-transition: rotate(45deg);
}
```

In order to essentially create a chevron by just rotating a box. I was having trouble getting this to actually appear so I hacked a chevron in by just adding an icon after the `select` input inside the `select-wrapper` element. I'm not really happy with it because while it looks correct, clicking on the actual chevron yields no results from the select input. If I had a bit more time, I'd want to get this actually working as intended but I'm up against my time box and its time to call it.

Time Spent:

- Custom Select: ~1 hour
- App Setup: ~20 minutes
- Add / Remove / Reset Rows: ~10 minutes
- Query Generation: ~1 hour
- Data Architecture: ~30 minutes
- Dynamically Generating Rows: ~1.5 hours

To Run the project:

```
$ git clone git@gitlab.com:erahm/quantum.git
$ cd quantum
$ npm install
$ npm start
```
